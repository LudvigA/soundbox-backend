﻿using Microsoft.EntityFrameworkCore;
using SoundBoxApi.Models;
using SoundBoxApi.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoundBoxApi.Services.ReviewService
{
    public class ReviewService : IReviewService
    {
        private readonly SoundBoxDbContext _context;

        public ReviewService(SoundBoxDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Review>> GetReviewsAsync()
        {
            return await _context.Reviews.ToListAsync();
        }

        public async Task<Review> GetReviewAsync(int id)
        {
            return await _context.Reviews.Where(g => g.Id == id).FirstAsync();
        }

        public async Task PutReviewAsync(Review review)
        {
            _context.Entry(review).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task PostReviewAsync(Review review)
        {
            _context.Reviews.Add(review);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteReview(Review review)
        {
            _context.Reviews.Remove(review);
            await _context.SaveChangesAsync();
        }

        public bool ReviewExists(int id)
        {
            return _context.Reviews.Any(e => e.Id == id);
        }



    }
}
