﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SoundBoxApi.Models.Domain;

namespace SoundBoxApi.Services.ReviewService
{
   public interface IReviewService
    {
        public Task<IEnumerable<Review>> GetReviewsAsync();
        public Task<Review> GetReviewAsync(int id);
        public Task PutReviewAsync(Review review);
        public Task PostReviewAsync(Review review);
        public Task DeleteReview(Review review);
        public bool ReviewExists(int id);

    }
}
