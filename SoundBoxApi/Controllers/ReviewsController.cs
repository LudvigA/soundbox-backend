﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SoundBoxApi.Models;
using SoundBoxApi.Models.Domain;
using SoundBoxApi.Models.DTO.ReviewDTO;
using SoundBoxApi.Services.ReviewService;

namespace SoundBoxApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class ReviewsController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly IReviewService _reviewService;

        public ReviewsController(IMapper mapper, IReviewService reviewService)
        {
            _mapper = mapper;
            _reviewService = reviewService;
        }

        // GET: api/Reviews
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReviewReadDTO>>> GetReviews()
        {
            return _mapper.Map<List<ReviewReadDTO>>(await _reviewService.GetReviewsAsync());
        }

        // GET: api/Reviews/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReviewReadDTO>> GetReview(int id)
        {
            try
            {
                var review = _mapper.Map<ReviewReadDTO>(await _reviewService.GetReviewAsync(id));
                return review;
            }
            catch
            {
                return NotFound();
            }
        }

        // PUT: api/Reviews/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReview(int id, ReviewUpdateDTO review)
        {
            if (id != review.Id)
            {
                return BadRequest();
            }
      
            if (!_reviewService.ReviewExists(id))
            {
                return NotFound();
            }

            Review domainReview = _mapper.Map<Review>(review);
            await _reviewService.PutReviewAsync(domainReview);

            return NoContent();
        }

        // POST: api/Reviews
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Review>> PostReview(ReviewCreateDTO review)
        {
            try
            {
                Review domainReview = _mapper.Map<Review>(review);
                await _reviewService.PostReviewAsync(domainReview);

                return CreatedAtAction("GetReview", new { id = domainReview.Id }, domainReview);
            }
            catch
            {
                return NotFound();
            }
        }

        // DELETE: api/Reviews/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReview(int id)
        {
            try
            {
                var review = await _reviewService.GetReviewAsync(id);
                await _reviewService.DeleteReview(review);
                return NoContent();
            }
            catch
            {
                return NotFound();
            }
        }

    }
}
