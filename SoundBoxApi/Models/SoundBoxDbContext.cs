﻿using Microsoft.EntityFrameworkCore;
using SoundBoxApi.Models.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace SoundBoxApi.Models
{
    public class SoundBoxDbContext : DbContext
    {
        //Tables
        public DbSet<Review> Reviews { get; set; }

        public SoundBoxDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

        }
    }

}
