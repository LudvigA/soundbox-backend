﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SoundBoxApi.Models.DTO.ReviewDTO
{
    public class ReviewReadDTO
    {
        public int Id { get; set; }
        public string ArtistName { get; set; }
        public string SongName { get; set; }
        public string ReviewText { get; set; }
        public int Rating { get; set; }
        public string SpotifyLink { get; set; }
        public string ReviewerName { get; set; }
    }
}
