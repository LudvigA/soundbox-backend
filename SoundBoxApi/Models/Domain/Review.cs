﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SoundBoxApi.Models.Domain
{
    public class Review
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string ArtistName { get; set; }

        [Required]
        [MaxLength(50)]
        public string SongName { get; set; }

        [Required]
        [MaxLength(250)]
        public string ReviewText { get; set; }

        [Required]
        public int Rating { get; set; }

        [Required]
        public string SpotifyLink { get; set; }
        
        [Required]
        [MaxLength(25)]
        public string ReviewerName { get; set; }
    }
}
